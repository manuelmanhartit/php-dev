# Php Development Environment 1.1

The Docker setup for PHP applications using PHP7-FPM and Nginx like described [here](http://geekyplatypus.com/dockerise-your-php-application-with-nginx-and-php7-fpm)

You have to create an `.env` file for the configuration of the container to fit your local machine.

If you have more generic questions please have a look [here](https://cms.manhart.space/dockerisierung)

## Getting Started

Copy the `.env-example` file into a `.env` file and edit the variables to fit your environment.

    # cp .env-example .env
    # vi .env

Start the container with the command:

    # sudo docker-compose up -d

Your PHP development server is available on 127.0.0.1:8081

### Manual steps

-

### Build / Extend

You can eg. add more volume mounts as you need.

## Serve resources via php

If you want to serve some resources eg. images over you php scripts (because the dir is not accessible via nginx)

1. Add this to your nginx config

		location ^~ /images/ {
			try_files $uri /index.php?f=$uri&$args;
			add_header        Cache-Control public;
			add_header        Cache-Control must-revalidate;
			expires max;
		}

2. Add a host mounted volume mapping your resources

3. Add the corresponding host headers in your php script

### Replicating the service

Since this is purely a dev environment replicating should be easy via copying everything and adapting the LOCAL PORTS.

### Upgrading the service / containers

For upgrading the container, you will need to call

	# docker-compose down
	
to tear down the service.

If you want to change the PHP (main) version (currently 7.x) you have to adapt the compose file.

Then pull the latest version with

	# docker-compose pull

Lastly start the service with

	# docker-compose up -d

## About the project

### Versioning

We use MAIN.MINOR number for versioning where as MAIN usually means big / breaking changes and MINOR usually means small / non breaking changes. For the version number, see at the top header or in the [tags on this repository](https://bitbucket.org/mmprivat/php-dev/downloads/?tab=tags).

### Author(s)

* **Manuel Manhart** - *Initial work*

### License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

### Contributing

If you want to contribute, contact the author.

### Changelog

__1.1__

* Updated readme
* Changed license
* Put into public bitbucket repo
* Updated image versions to more generic ones

### Open issues

-

## Sources

* [Docker](http://www.docker.io/) - The container base
* [Docker-Compose](https://docs.docker.com/compose/) - Composing multiple containers into one service
* [VS Code](https://code.visualstudio.com/) - Used to edit all the files
* [Nginx Image](https://hub.docker.com/_/nginx)
* [Php Image](https://hub.docker.com/_/php)
* [Activate PHP logging](https://github.com/mikechernev/dockerised-php/tree/feature/log-to-stdout)
* [Adding a Healthcheck](https://howchoo.com/g/zwjhogrkywe/how-to-add-a-health-check-to-your-docker-container)
